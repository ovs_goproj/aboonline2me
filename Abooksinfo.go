package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
)

const (
	url_playlist                    = "https://abooks.info/?audioigniter_playlist_id="
	r_id_template_abooksinfo string = `<div id="[a-z]+-(?P<Id>\d+?)"\sclass="audioigniter-root"\sdata-player-type`
	name_template_abooksinfo string = `<h1 class="entry-title">(?P<Name>.+?)</h1>`
)

type Abooksinfo struct {
	html       string
	url        string
	abook_name string
	jobs       []URLJob
	results    []URLResult
}

func (a *Abooksinfo) GetTitle() (string, error) {
	var (
		err error
		str []string
	)

	if a.html == "" {
		a.html, err = _get_html(a.url)
		if err != nil {
			return "", errors.New("Не удается получить страницу аудиокниги")
		}
	}

	r_title := regexp.MustCompile(name_template_abooksinfo)
	str = r_title.FindStringSubmatch(a.html)
	if len(str) < 2 {
		return "", errors.New("Не удалось найти название книги!")
	}

	a.abook_name = str[1]

	return str[1], nil
}

func (a *Abooksinfo) Parse() ([]URLJob, error) {
	var err error
	var bjson []byte

	if a.jobs != nil {
		return a.jobs, nil
	}

	if a.html == "" {
		a.html, err = _get_html(a.url)
		if err != nil {
			return nil, errors.New("Не удается получить страницу аудиокниги")
		}
	}

	r_url := regexp.MustCompile(r_id_template_abooksinfo)
	str := r_url.FindStringSubmatch(a.html)
	if len(str) < 2 {
		return nil, errors.New("Не удалось найти название книги!")
	}

	plst := fmt.Sprintf("%s%s", url_playlist, str[1])

	bjson, err = _get(plst)
	if err != nil {
		return nil, errors.New("Не удается получить данные с сайта аудиокниги")
	}

	type item_abooks struct {
		Title    string `json:"title"`
		Subtitle string `json:"subtitle"`
		Audio    string `json:"audio"`
	}
	var items []item_abooks

	e := json.Unmarshal(bjson, &items)
	if e != nil {
		return nil, errors.New("Не удалось разобрать список для загрузки!")
	}

	// подготовим список работы
	var urlJobList = make([]URLJob, 0, len(items))
	for _, i := range items {
		urlJobList = append(urlJobList,
			URLJob{url: i.Audio, file: fmt.Sprintf("%s-%s.mp3", i.Title, i.Subtitle)})
	}

	a.jobs = urlJobList

	return urlJobList, nil
}
