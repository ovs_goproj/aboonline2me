module aboonline2me

require (
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/microcosm-cc/bluemonday v1.0.2
	github.com/tcolgate/gostikkit/evpkdf v0.0.0-20181107100648-5e18a232dc98
	gopkg.in/go-playground/validator.v9 v9.25.0
)
