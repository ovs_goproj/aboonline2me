package main

import (
	"io/ioutil"
	"net/http"
	"strings"
)

type Header struct {
	Name  string
	Value string
}

func _get_html(url string) (string, error) {
	b, e := _get(url)
	if e != nil {
		return "", e
	}
	html := string(b)

	return html, nil
}

func _post(url string, data *strings.Reader, headers ...Header) ([]byte, error) {
	var (
		resp *http.Response
		req  *http.Request
		err  error
		ret  []byte
	)

	client := &http.Client{}
	req, err = http.NewRequest("POST", url, data)
	if err != nil {
		return nil, err
	}

	req.Header.Set("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36")

	for _, h := range headers {
		req.Header.Set(h.Name, h.Value)
	}

	resp, err = client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	ret, err = ioutil.ReadAll(resp.Body)
	return ret, err
}

func _get(url string) ([]byte, error) {
	var (
		resp *http.Response
		req  *http.Request
		err  error
		ret  []byte
	)

	client := &http.Client{}

	req, err = http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36")

	resp, err = client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	ret, err = ioutil.ReadAll(resp.Body)

	return ret, err
}

func fetch(url string, file string) URLResult {
	body, err := _get(url)
	if err != nil {
		return URLResult{URL: url, Err: err}
	}

	e := ioutil.WriteFile(file, body, 0644)
	if e != nil {
		return URLResult{URL: url, Err: e}
	}

	//fmt.Println("Получено по ссылке", url)

	return URLResult{URL: url, Err: nil}
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// общий интерфейс для обработчиков всех сайтов
type JobPrepareInterface interface {
	// распарсить HTML и получить слайс работ
	Parse() ([]URLJob, error)
	// распарсить и получить имя книги
	GetTitle() (string, error)
}
