package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
)

const (
	r_template_knigavuhe    string = `\svar\splayer\s=\snew\sBookPlayer\(\d+,\s(?P<Urls>\[.+?\]).+\);`
	name_template_knigavuhe string = `<span class="book_title_elem book_title_name" itemprop="name">(?P<Name>.+?)</span>`
)

type Knigavuhe struct {
	html       string
	url        string
	abook_name string
	jobs       []URLJob
	results    []URLResult
}

func (k *Knigavuhe) Parse() ([]URLJob, error) {
	var err error

	if k.jobs != nil {
		return k.jobs, nil
	}

	if k.html == "" {
		k.html, err = _get_html(k.url)
		if err != nil {
			return nil, errors.New("Не удается получить страницу аудиокниги")
		}
	}

	r := regexp.MustCompile(r_template_knigavuhe)
	str := r.FindStringSubmatch(k.html)
	if len(str) < 2 {
		return nil, errors.New("Не удалось найти список для загрузки!")
	}

	b := []byte(str[1])
	type Item_knigavuhe struct {
		Title string `json:"title"`
		Url   string `json:"url"`
	}
	var items []Item_knigavuhe

	e := json.Unmarshal(b, &items)
	if e != nil {
		return nil, errors.New("Не удалось разобрать список для загрузки!")
	}

	// подготовим список работы
	var urlJobList = make([]URLJob, 0, len(items))
	for _, i := range items {
		urlJobList = append(urlJobList,
			URLJob{url: i.Url, file: fmt.Sprintf("%s.mp3", i.Title)})
	}

	k.jobs = urlJobList

	return urlJobList, nil
}

func (k *Knigavuhe) GetTitle() (string, error) {
	var (
		err error
		str []string
	)

	if k.html == "" {
		k.html, err = _get_html(k.url)
		if err != nil {
			return "", errors.New("Не удается получить страницу аудиокниги")
		}
	}

	r_title := regexp.MustCompile(name_template_knigavuhe)
	str = r_title.FindStringSubmatch(k.html)
	if len(str) < 2 {
		return "", errors.New("Не удалось найти название книги!")
	}

	k.abook_name = str[1]

	return str[1], nil
}
