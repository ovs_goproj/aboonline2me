package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"net/url"
	"os"
	"strings"

	"gopkg.in/go-playground/validator.v9"
)

const (
	version string = "1.0"
)

var (
	WORKERS int    = 4  //кол-во "потоков"
	URL     string = "" // URL аудиокниги
	DIR     string = "" // Директория куда сохранить каталог с аудиофайлами

	known_sites []string = []string{
		"knigavuhe.com",
		"abooks.info",
		"audioknigi.club",
	}
	validate *validator.Validate
)

func init() {
	var err error
	DIR, err = os.Getwd()
	if err != nil {
		log.Fatal(errors.New("Не удалось настроить текушую директорию!"))
	}
	validate = validator.New()
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(),
			"Загрузка аудиокниг со специальзованных сайтов. Доступны следующие сайты:\n\t%s.\nИспользование приложения %s v%s:\n",
			strings.Join(known_sites, ", "), os.Args[0], version)
		flag.PrintDefaults()
	}

	flag.IntVar(&WORKERS, "w", WORKERS, "количество потоков")
	flag.StringVar(&DIR, "d", DIR, "директория сохранения")
	flag.StringVar(&URL, "u", URL, "URL аудиокниги")
	flag.Parse()

	u, err := url.Parse(URL)
	if err != nil {
		log.Fatal("Это не URL!")
	}
	if !((u.Scheme == "http") || (u.Scheme == "https")) {
		log.Fatal("Это не HTTP/HTTPS URL!")
	}

	if !(stringInSlice(u.Host, known_sites)) {
		log.Fatal("Этот сайт нам неизвестен!")
	}

	var e error
	var i JobPrepareInterface
	switch u.Host {
	case "knigavuhe.com":
		i = &Knigavuhe{url: URL}
	case "abooks.info":
		i = &Abooksinfo{url: URL}
	case "audioknigi.club":
		i = &Audioknigiclub{url: URL}
	default:
		e = errors.New("Ошибка запуска обработки сайта книги!")
	}

	e = Run(i, WORKERS)
	if e != nil {
		log.Fatal(e)
	}
}
