package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path"

	"github.com/microcosm-cc/bluemonday"
)

var saintizer *bluemonday.Policy = bluemonday.StrictPolicy()

type URLJob struct {
	url  string
	file string
}

type URLResult struct {
	URL string
	Err error
}

func Run(iface JobPrepareInterface, parallelLimit int) error {
	if iface == nil {
		return errors.New("На вход не получено задания!")
	}

	var (
		book_title string
		err        error
		target     string
	)

	book_title, err = iface.GetTitle()
	if err != nil {
		return err
	}

	target = path.Join(DIR, saintizer.Sanitize(book_title))
	err = os.MkdirAll(target, 0755)
	if err != nil {
		return err
	}

	fmt.Printf("Загружается '%s' в каталог '%s'...\n", book_title, DIR)

	// получим список работы от нужного плагина
	urlJobList := make([]URLJob, 0)
	urlJobList, err = iface.Parse()
	if err != nil {
		return err
	}

	totalJobs := len(urlJobList)

	if parallelLimit > totalJobs {
		parallelLimit = totalJobs
	}

	jobs := make(chan URLJob, parallelLimit)
	results := make(chan URLResult, totalJobs)

	urlResultList := make([]URLResult, 0, totalJobs)

	// запустим обработчки в нужном колличестве
	for w := 1; w <= parallelLimit; w++ {
		go worker(w, target, jobs, results)
	}

	// отправим в канал работу
	go to_workers(urlJobList, jobs)

	fmt.Printf("\rПолучено: %d/%d", 0, totalJobs)

	var count int = 1
	for i := 0; i < totalJobs; i++ {
		select {
		case urlResult := <-results:
			fmt.Printf("\rПолучено: %d/%d", count, totalJobs)
			urlResultList = append(urlResultList, urlResult)
			count++
		}
	}

	var errs []string = make([]string, 0)
	for _, res := range urlResultList {
		if res.Err != nil {
			errs = append(errs, res.URL)
		}
	}

	if len(errs) > 0 {
		jsonBytes, err := json.MarshalIndent(errs, "", "    ")
		if err != nil {
			return err
		}
		fmt.Println()
		fmt.Println("При загрузке были ошибки. Список URL с ошибками:")
		fmt.Println(string(jsonBytes))
	} else {
		fmt.Printf("\nВсё готово!\n")
	}

	return nil
}

func worker(id int, target string, jobs <-chan URLJob, results chan<- URLResult) {
	var (
		err   error
		fpath string
	)

	// TODO: check file as unique
	for j := range jobs {
		err = validate.Var(j.url, "required,url")
		if err != nil {
			results <- URLResult{URL: j.url, Err: err}
			continue
		}
		err = validate.Var(j.file, "required,min=1")
		if err != nil {
			results <- URLResult{URL: j.url, Err: err}
			continue
		}
		fpath = path.Join(target, saintizer.Sanitize(j.file))
		results <- fetch(j.url, fpath)
	}
}

func to_workers(urlJobList []URLJob, jobs chan URLJob) {
	for _, urlJob := range urlJobList {
		jobs <- urlJob
	}
}
