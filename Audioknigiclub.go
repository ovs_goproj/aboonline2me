/*
Есть какой-то мутный GET
https://audioknigi.club/engine/lib/external/kcaptcha/index.php?PHPSESSID=jto0e94i7tqil5adpu3l3j8al6
*/

package main

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/tcolgate/gostikkit/evpkdf"
	"net/url"
	"regexp"
	"strings"
)

const (
	url_playlist_audioknigiclub    string = "https://audioknigi.club/ajax/bid/" // +id
	r_bid_template_audioknigiclub  string = `\$\(document\)\.audioPlayer\((?P<ID>\d+?),\d+\);`
	r_name_template_audioknigiclub string = `<link.*title="(?P<name>.+?)"><link`
	r_sessid_audioknigiclub        string = `var\s{1,}SESSION_ID\s{1,}=\s{1,}'(?P<sessid>.+?)';`
	r_secls_key_audioknigclub      string = `var\s{1,}LIVESTREET_SECURITY_KEY\s{1,}=\s{1,}'(?P<sec_key>.+?)';`
	acl_err                        string = `https://get.sweetbook.net/acl.mp3`
	drm_passwd                     string = "EKxtcg46V"
)

type Audioknigiclub struct {
	html       string
	url        string
	abook_name string
	jobs       []URLJob
	results    []URLResult
}

func (a *Audioknigiclub) GetTitle() (string, error) {
	var (
		err error
		str []string
	)

	if a.html == "" {
		a.html, err = _get_html(a.url)
		if err != nil {
			return "", errors.New("Не удается получить страницу аудиокниги")
		}
	}

	r_title := regexp.MustCompile(r_name_template_audioknigiclub)
	str = r_title.FindStringSubmatch(a.html)
	if len(str) < 2 {
		return "", errors.New("Не удалось найти название книги!")
	}
	a.abook_name = str[1]

	return str[1], nil
}

func (a *Audioknigiclub) Parse() ([]URLJob, error) {
	var (
		err    error
		bjson  []byte
		str    []string
		params url.Values = url.Values{}
	)

	if a.jobs != nil {
		return a.jobs, nil
	}

	if a.html == "" {
		a.html, err = _get_html(a.url)
		if err != nil {
			return nil, errors.New("Не удается получить страницу аудиокниги")
		}
	}

	// получить secret_key
	r_sec_key := regexp.MustCompile(r_secls_key_audioknigclub)
	str = r_sec_key.FindStringSubmatch(a.html)
	if len(str) < 2 {
		return nil, errors.New("Не удалось разобрать страницу аудиокниги!")
	}

	sec_key := str[1]
	params.Set("security_ls_key", sec_key)

	// получить bid
	r_bid := regexp.MustCompile(r_bid_template_audioknigiclub)
	str = r_bid.FindStringSubmatch(a.html)
	if len(str) < 2 {
		return nil, errors.New("Не удалось разобрать страницу аудиокниги!")
	}

	bid := str[1]
	params.Set("bid", bid)

	// итоговый URL для получения playlist
	plst_url := fmt.Sprintf("%s%s", url_playlist_audioknigiclub, bid)

	// получить PHPSESSID
	r_sessid := regexp.MustCompile(r_sessid_audioknigiclub)
	str = r_sessid.FindStringSubmatch(a.html)
	if len(str) < 2 {
		return nil, errors.New("Не удалось разобрать страницу аудиокниги!")
	}

	sessid := str[1]
	cookie := Header{Name: "Cookie", Value: fmt.Sprintf("PHPSESSID=%s;", sessid)}

	hdr := Header{Name: "Content-Type", Value: "application/x-www-form-urlencoded"}

	var hash string
	hash, err = hash_stringify(sec_key, drm_passwd)
	if err != nil {
		return nil, errors.New("Не удалось подготовить данные для отправики запроса!")
	}
	params.Set("hash", hash)

	bjson, err = _post(plst_url, strings.NewReader(params.Encode()), hdr, cookie)
	if err != nil {
		return nil, errors.New("Не удается получить данные с сайта аудиокниги")
	}

	type items_audioknigiclub_string struct {
		Items string `json:"aItems"`
	}

	items_string := new(items_audioknigiclub_string)
	err = json.Unmarshal(bjson, items_string)
	if err != nil {
		return nil, errors.New("Не удалось разобрать список для загрузки, 1!")
	}

	type item_audioknigiclub struct {
		Title string `json:"title"`
		Audio string `json:"mp3"`
	}
	var items []item_audioknigiclub

	err = json.Unmarshal([]byte(items_string.Items), &items)
	if err != nil {
		return nil, errors.New("Не удалось разобрать список для загрузки, 2!")
	}

	if len(items) < 1 {
		return nil, errors.New("Список для загрузки пуст!")
	}
	if items[0].Audio == acl_err {
		return nil, errors.New("ACL ошибка!")
	}

	// подготовим список работы
	var urlJobList = make([]URLJob, 0, len(items))
	for _, i := range items {
		urlJobList = append(urlJobList,
			URLJob{url: i.Audio, file: fmt.Sprintf("%s.mp3", i.Title)})
	}

	a.jobs = urlJobList

	return urlJobList, nil
}

// from https://github.com/tcolgate/gostikkit/blob/master/crypto.go

// Appends padding.
func pkcs7Pad(data []byte, blocklen int) ([]byte, error) {
	if blocklen <= 0 {
		return nil, fmt.Errorf("invalid blocklen %d", blocklen)
	}
	padlen := uint8(1)
	for ((len(data) + int(padlen)) % blocklen) != 0 {
		padlen++
	}

	if int(padlen) > blocklen {
		panic(fmt.Sprintf("generated invalid padding length %v for block length %v", padlen, blocklen))
	}
	pad := bytes.Repeat([]byte{byte(padlen)}, int(padlen))
	return append(data, pad...), nil
}

func encrypt(plaintext, password string) ([]byte, []byte, []byte, error) {
	salt := genChars(8)

	keylen := 32
	key := make([]byte, keylen)

	ivlen := aes.BlockSize
	iv := make([]byte, ivlen)
	rand.Read(iv)

	keymat := evpkdf.New(md5.New, []byte(password), salt, keylen+ivlen, 1)
	keymatbuf := bytes.NewReader(keymat)

	n, err := keymatbuf.Read(key)
	if n != keylen || err != nil {
		return nil, nil, nil, errors.New("keymaterial was short reading key")
	}

	n, err = keymatbuf.Read(iv)
	if n != ivlen || err != nil {
		return nil, nil, nil, errors.New("keymaterial was short reading iv")
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	padded, err := pkcs7Pad([]byte(plaintext), block.BlockSize())
	if err != nil {
		return nil, nil, nil, errors.New("padding blew up, " + err.Error())
	}

	ciphertext := make([]byte, len(padded))
	mode := cipher.NewCBCEncrypter(block, iv)
	mode.CryptBlocks(ciphertext, padded)

	return ciphertext, iv, salt, nil
}

func hash_stringify(security_ls_key, drm_passwd string) (string, error) {
	ls_key := fmt.Sprintf("\"%s\"", security_ls_key)

	ct, iv, salt, err := encrypt(ls_key, drm_passwd)
	if err != nil {
		return "", err
	}

	iv_hex := hex.EncodeToString(iv)
	salt_hex := hex.EncodeToString(salt)

	return fmt.Sprintf("{\"ct\":\"%s\",\"iv\":\"%s\",\"s\":\"%s\"}",
		base64.StdEncoding.EncodeToString([]byte(ct)),
		iv_hex, salt_hex), nil
}

var chars = []byte("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

func genChars(n int) []byte {
	out := make([]byte, n)
	rs := make([]byte, n)
	rand.Read(rs)
	for i := 0; i < n; i++ {
		out[i] = chars[uint(rs[i])%uint(len(chars))]
	}
	return out
}
